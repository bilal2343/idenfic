<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = $request->user();

        if ($user->status === 'pending') {
            return response()->json(['message' => 'You are not a verified user.'], 403);
        }

        if ($user->status === 'inactive') {
            return response()->json(['message' => 'You are de-activated by the admin.'], 403);
        }

        return $next($request);
    }
}
