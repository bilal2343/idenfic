<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(UserRequest $request)
    {
        $user = User::create([
            'user_name' => $request->input('user_name'),
            'phone_no' => $request->input('phone_no'),
            'password' => bcrypt($request->input('password')),
            'otp' => rand(1000, 9999),
        ]);

        return response()->json([
            'message' => 'OTP sent successfully.',
            'user' => $user,
        ], 201);
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['user_name', 'password']);

        if (!Auth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $token = Auth::user()->createToken('auth_token')->plainTextToken;
        return response()->json([
            'message' => 'User Logged In successfully.',
            'access_token' => $token,
            'token_type' => 'Bearer',
        ]);
    }

    public function verifyOtp(Request $request)
    {
        $user = User::where('phone_no', $request->input('phone_no'))->first();

        if (!$user) {
            return response()->json(['error' => 'User not found.'], 404);
        }

        if ($user->otp != $request->input('otp')) {
            return response()->json(['error' => 'Invalid OTP.'], 422);
        }

        $user->status = 'active';
        $user->otp = null;
        $user->update();

        return response()->json([
            'message' => 'OTP verified successfully.',
        ]);
    }

    public function sendOtp(Request $request)
    {
        $request->validate([
            'phone_no' => ['required', 'required|numeric|digits:11'],
        ]);

        $user = User::where('phone_no', $request->phone_no)->first();

        if (!$user) {
            return response()->json(['message' => 'User not found.'], 404);
        }

        $user->otp = rand(1000, 9999);
        $user->update();

        return response()->json(['message' => 'OTP sent successfully.'], 200);
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'phone_no' => ['required', 'required|numeric|digits:11'],
            'password' => 'required|min:8|max:12|confirmed',
        ]);

        $user = User::where('phone_no', $request->phone_no)->first();

        if (!$user) {
            return response()->json(['message' => 'User not found.'], 404);
        }

        $user->password = bcrypt($request->password);
        $user->update();

        return response()->json(['message' => 'Password reset successfully.'], 200);
    }

}
