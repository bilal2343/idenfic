<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules()
    {
        return [
            'user_name' => 'required|string|max:255|unique:users',
            'phone_no' => 'required|numeric|digits:11|unique:users',
            'password' => 'required|string|min:8|max:12|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'user_name.required' => 'The user name field is required.',
            'user_name.max' => 'The user name may not be greater than 255 characters.',
            'user_name.unique' => 'The user name is already in use.',
            'phone_no.required' => 'The phone number field is required.',
            'phone_no.numeric' => 'The phone number must be a number.',
            'phone_no.digits' => 'The phone number must be exactly 11 digits.',
            'phone_no.unique' => 'The phone number is already in use.',
            'password.required' => 'The password field is required.',
            'password.min' => 'The password must be at least 8 characters.',
            'password.max' => 'The password may not be greater than 12 characters.',
            'password.confirmed' => 'The password confirmation does not match.',
        ];
    }
}
